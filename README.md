# Less Parser #


## Features ##

* Grammar compatible with [Less Grammar](http://lesscss.org/features/)
* Whitespace insensitive (in places where whitespaces don't matter)
* Error detection
* Error message with the exact position of error and with expected values (tokens)
* Error message includes snippet from a file of restricted length with an error properly highlighted


test.txt filename is hardcoded filename that should be put in a project directory (on src/ directory level)