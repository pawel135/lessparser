package main.string_utils;

public class StringUtils {
    private static final int STRING_LENGTH_BEFORE = 30;
    private static final int STRING_LENGTH_AFTER = 30;
    private static final String ERROR_START_SYMBOL = "[{";
    private static final String ERROR_END_SYMBOL = "}]";

    public static String getTextWithHighlighted(String text, int errorStart, int errorEnd){
        String beginning = "";
        String ending = "";
        int returnStringStart = 0;
        int returnStringEnd = text.length(); //exclusive

        if ( errorStart - STRING_LENGTH_BEFORE > 0 ){
            returnStringStart = errorStart - STRING_LENGTH_BEFORE;
            beginning = "[...]";
        }
        if ( errorEnd + STRING_LENGTH_AFTER < text.length() ){
            returnStringEnd = errorEnd + STRING_LENGTH_AFTER;
            ending = "[...]";
        }
        return beginning+text.substring(returnStringStart,errorStart)+
                ERROR_START_SYMBOL +
                (errorStart < errorEnd ? text.substring(errorStart, errorEnd)  : "") + ERROR_END_SYMBOL +
                (errorEnd <= returnStringEnd ? text.substring(errorEnd, returnStringEnd): "") + ending;
    }
}

