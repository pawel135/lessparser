package main.tokenizer;

public class TokenUnidentifiableException extends Throwable {
    private int position;
    public TokenUnidentifiableException(int position){
        this.position = position;
    }
}
