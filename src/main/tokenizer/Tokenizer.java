package main.tokenizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Tokenizer {
    private final String text;
    private int head;

    public Tokenizer(String text){
        this.text = text;
    }

    private Token extractToken() { // throws TokenUnidentifiableException
        Token matchingToken = null;
        while( head < text.length() ) {
            for (TokenType tokenType : TokenType.values()) {
                Pattern pattern = tokenType.regexPattern;
                Matcher matcher = pattern.matcher(text);
                if (matcher.find(head)) {
                    if (matcher.start() == head && (matchingToken == null || matcher.end() > matchingToken.getEnd())) {
                        //System.out.println(tokenType.toString() + ", head: " + head);
                        matchingToken = new Token(tokenType, matcher.start(), matcher.end());
                    }
                }
            }
            if (matchingToken != null && matchingToken.getPattern().toString().length() > 0) {
                head = matchingToken.getEnd();
                return matchingToken;
            } else {
                //throw new TokenUnidentifiableException(head);
                head++;
            }
        }
        return null;
    }

    public List<Token> getTokens(){
        List<Token> tokens = new ArrayList<>();
        Token token;
        while ((token = extractToken()) != null){
            tokens.add(token);
        }
        tokens.add(new Token(TokenType.EOF, text.length(), text.length()+1));
        return tokens;
    }

    public static String getRegexForAllBut(TokenType... excludedTokenTypes){
        return Arrays.stream(TokenType.values())
                .filter(tt -> Arrays.stream(excludedTokenTypes)
                        .noneMatch(excludedTT -> tt==excludedTT) )
                .map(tt -> tt.regexPattern.pattern() )
                .collect(Collectors.joining("|"));
    }

    public static String getRegexFor(TokenType... includedTokenTypes){
        return Arrays.stream(TokenType.values())
                .filter(tt -> Arrays.stream(includedTokenTypes)
                        .anyMatch(includedTT -> tt==includedTT) )
                .map(tt -> tt.regexPattern.pattern() )
                .collect(Collectors.joining("|"));
    }

    public static TokenType[] getAllTokenTypesBut(TokenType... excludedTokenTypes){
        return Arrays.stream(TokenType.values())
                .filter(tt -> Arrays.stream(excludedTokenTypes)
                        .noneMatch(excludedTT -> tt==excludedTT) )
                .toArray(TokenType[]::new);
    }

    public static TokenType[] getAllTokenTypes(){
        return TokenType.values();
    }

}
