package main.tokenizer;

import java.util.regex.Pattern;

public enum TokenType{
    EPS( "" ),
    EOF( "\\Z" ),
    EOL( "\n|\r\n" ),
    SEMICOLON( ";" ),
    COLON( ":" ),
    AT( "@" ),
    SPECIAL_CHARS( "!|\\?|<|ą|ę|ó|ś|ł|ż||ź|ć|ń" ),
    IMPORT_KEYWORD( "@import" ),
    INLINE_COMMENT_OPEN( "//" ),
    BLOCK_COMMENT_OPEN( "/\\*" ),
    BLOCK_COMMENT_CLOSE( "\\*/" ),
    UNIT( "px|em|%" ),
    PLUS( "\\+" ),
    MINUS( "\\-" ),
    SHARP_CLOSE( ">" ),
    SLASH( "/" ),
    EQUALS( "\\=" ),
    TILDE(  "~" ),
    PIPE( "\\|" ),
    PARENTHESIS_OPEN( "\\(" ),
    PARENTHESIS_CLOSE( "\\)" ),
    BRACKETS_OPEN( "\\{" ),
    BRACKETS_CLOSE( "\\}" ),
    BRACES_OPEN( "\\[" ),
    BRACES_CLOSE( "\\]" ),
    ASTERISK( "\\*" ),
    HASH( "#" ),
    AMPERSAND( "\\&" ),
    CARET( "\\^" ),
    DOLLAR( "\\$" ),
    DOT( "\\." ),
    COMMA( "\\," ),
    SINGLE_QUOTE( "\'" ),
    DOUBLE_QUOTE( "\"" ),
    UNDERSCORE( "_" ),
    LETTER( "[a-zA-Z]" ),
    DIGIT( "[0-9]" ),
    WHITESPACES( "[ \t\f]+" );

/*    NUMBER( "[0-9]+\\.?[0-9]*" ),
    HEX_VALUE( "#([0-9a-fA-F]{1,2}){3}" ),
    SELECTORS_SEPARATOR( "(\\s+)|(\\s*[\\+\\,\\(]\\s*)" ),
*/

    TokenType(String regex){
        this.regexPattern = Pattern.compile(regex);
        //this.regexPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    }
    public final Pattern regexPattern;

}
