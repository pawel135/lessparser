package main.tokenizer;

import java.util.regex.Pattern;

public class Token {
    private TokenType tokenType;
    private int start;
    private int end;

    public Token(TokenType tokenType, int start, int end){
        this.tokenType = tokenType;
        this.start = start;
        this.end = end;
    }

    public Pattern getPattern(){
        return tokenType.regexPattern;
    }

    public int getStart(){
        return start;
    }

    public void setStart(int start){
        this.start = start;
    }

    public int getEnd(){
        return end;
    }

    public void setEnd(int end){
        this.end = end;
    }

    public TokenType getTokenType(){
        return tokenType;
    }

    public void setTokenType(TokenType tokenType){
        this.tokenType = tokenType;
    }

    @Override
    public String toString(){
        return tokenType.toString() + ", start: "+ start + ", end: " + end;
    }
}

