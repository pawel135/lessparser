package main;

import main.parser.ParseException;
import main.parser.Parser;
import main.scanner.Scanner;
import main.tokenizer.Token;
import main.string_utils.StringUtils;

import java.io.FileNotFoundException;
import java.util.List;

public class Main {
    private static final String FILENAME_DEFAULT = "test.txt";

    public static void main(String... args){
        String filename = args.length > 1 ? args[1] : FILENAME_DEFAULT;
        Scanner scanner = null;
        try{
            scanner = new Scanner(filename);
        } catch (FileNotFoundException e) {
            System.err.println("Specified file doesn't exist");
            System.exit(-1);
        }
        String fileContent = scanner.getFileContentAsString();
        //System.out.println(fileContent);
        List<Token> tokens = scanner.getTokens();
        //System.out.println(tokens);
        Parser parser = new Parser(tokens);
        try{
            parser.parse();
        } catch( ParseException e){
            //System.err.println( e.toString() );
            System.err.println( e.toString(fileContent) );
            int errorStart = e.getUnexpectedStart();
            int errorEnd = e.getUnexpectedEnd();
            System.err.println( StringUtils.getTextWithHighlighted(fileContent, errorStart, errorEnd) );

        }
    }




}
