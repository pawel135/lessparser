package main.parser;

import main.tokenizer.Token;
import main.tokenizer.TokenType;
import main.tokenizer.Tokenizer;

import java.util.*;

public  class Parser {
    private final List<Token> tokens;
    private int tokensHead = 0;
    private Token currentToken;

    private Token farthestToken;
    private Set<TokenType> farthestTokenExpectedTypes;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
        currentToken = tokens.get(tokensHead);
        farthestTokenExpectedTypes = new TreeSet<>();
    }

    public void parse() throws ParseException {
        commands();
        if( currentToken.getTokenType() != TokenType.EOF){
            throw new ParseException(currentToken, TokenType.EOF);
        }
    }

    private void getToken(TokenType tokenType) throws ParseException {
        if (currentToken.getTokenType() != tokenType) {
            if ( farthestToken == null || currentToken.getEnd() >= farthestToken.getEnd()){
                if ( farthestToken != null && currentToken.getEnd() > farthestToken.getEnd() ){
                    farthestTokenExpectedTypes.clear();
                }
                farthestToken = currentToken;
                farthestTokenExpectedTypes.add(tokenType);
            }

            throw new ParseException(farthestToken, farthestTokenExpectedTypes.toArray(new TokenType[farthestTokenExpectedTypes.size()]));
            // if always only last matched error should be propagated: throw new ParseException(currentToken, tokenType);
        }
        try {
            currentToken = tokens.get(tokensHead+1);
            tokensHead++;
        } catch (IndexOutOfBoundsException e) {
            throw new ParseException(new Token(TokenType.EOF, tokensHead+1, tokensHead+1), tokenType);
        }
    }

    private void getAnyOfTokens(TokenType... tokenTypes) throws ParseException{
        for (TokenType tokenType : tokenTypes){
            try{
                getToken(tokenType);
                return;
            } catch(ParseException e){
                // maybe next is okay
            }
        }
        throw new ParseException(currentToken, tokenTypes);
    }

    //pops all consecutive whitespaces from the list of tokens where head is pointing at
    private void optionalWhitespaces() throws ParseException{
        popMatchingTokens(TokenType.WHITESPACES, TokenType.EOL);
    }

    private void requiredWhitespaces() throws ParseException{
        TokenType[] acceptedTokens = {TokenType.WHITESPACES, TokenType.EOL};
        popMatchingTokensRequired(acceptedTokens);
    }

    private void popMatchingTokens(TokenType... acceptedTokenTypes) throws ParseException {
        while (currentTokenTypeIsIn(acceptedTokenTypes)) {
            getToken(currentToken.getTokenType());
        }
    }

    private void popMatchingTokensRequired(TokenType... acceptedTokenTypes) throws ParseException {
        if (!currentTokenTypeIsIn(acceptedTokenTypes)) {
            throw new ParseException(currentToken, acceptedTokenTypes);
        }
        popMatchingTokens(acceptedTokenTypes);
    }

    private boolean currentTokenTypeIsIn(TokenType[] tokenTypes) {
        return Arrays.stream(tokenTypes).anyMatch(tt -> tt == currentToken.getTokenType());
    }


    private int saveState(){
        return this.tokensHead;
    }

    private void restoreState(int tokensHeadBackup){
        this.tokensHead = tokensHeadBackup;
        this.currentToken = tokens.get(tokensHead);
    }


    private void commands() throws ParseException {
        if ( currentToken.getTokenType() != TokenType.EOF ) {
            optionalWhitespaces();
            command();
            optionalWhitespaces();
            commands();
            optionalWhitespaces();
        }
    }


    private void command() throws ParseException {
        if (currentToken.getTokenType() == TokenType.AT) {
            variable_definition();
        } else if ( currentToken.getTokenType() == TokenType.IMPORT_KEYWORD ){
            import_statement();
        } else if (currentToken.getTokenType() == TokenType.INLINE_COMMENT_OPEN) {
            inline_comment();
        } else if (currentToken.getTokenType() == TokenType.BLOCK_COMMENT_OPEN) {
            block_comment();
        } else {
            element_style();
        }
    }

    private void import_statement() throws ParseException {
        getToken(TokenType.IMPORT_KEYWORD);
        requiredWhitespaces();
        string_text();
        optionalWhitespaces();
        getToken(TokenType.SEMICOLON);
    }

    private void string_text() throws ParseException {
        if (currentToken.getTokenType() == TokenType.SINGLE_QUOTE) {
            getToken(TokenType.SINGLE_QUOTE);
            text_in_string();
            getToken(TokenType.SINGLE_QUOTE);
        } else {
            getToken(TokenType.DOUBLE_QUOTE);
            text_in_string();
            getToken(TokenType.DOUBLE_QUOTE);
        }
    }

    private void text_in_string() throws ParseException {
        TokenType[] acceptedTokens = Tokenizer.getAllTokenTypesBut(TokenType.SINGLE_QUOTE, TokenType.DOUBLE_QUOTE);
        popMatchingTokens(acceptedTokens);
    }

    private void variable_definition() throws ParseException {
        variable_representation();
        optionalWhitespaces();
        getToken(TokenType.COLON);
        optionalWhitespaces();
        r_value();
        optionalWhitespaces();
        getToken(TokenType.SEMICOLON);
    }

    private void variable_representation() throws ParseException{
        getToken(TokenType.AT);
        variable_name();
    }
    private void variable_name() throws ParseException {
        name();
    }

    private void name() throws ParseException{
        name_start();
        name_end();
    }

    private void name_start() throws ParseException{
        if(currentToken.getTokenType()==TokenType.LETTER){
            getToken(TokenType.LETTER );
        } else{
            getToken(TokenType.UNDERSCORE );
        }
    }
    private void name_end() throws ParseException{
        TokenType[] acceptedTokens = {TokenType.LETTER, TokenType.DIGIT, TokenType.UNDERSCORE, TokenType.MINUS};
        popMatchingTokens(acceptedTokens);
    }

    private void r_values() throws ParseException{
        r_value();
        continuation_r_values();
    }

    private void continuation_r_values() {
        int tokensHeadBackup = saveState();
        try{
            values_separator();
            r_values();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            // eps
        }
    }

    private void values_separator() throws ParseException{
        requiredWhitespaces();
    }

    //<r_value>		::= <variable_representation> | <value> | <accessor> | <operation_section> | 			<mixin_call>
    private void r_value() throws ParseException {
        if( currentToken.getTokenType() == TokenType.PARENTHESIS_OPEN){
            getToken(TokenType.PARENTHESIS_OPEN);
            optionalWhitespaces();
            r_value();
            optionalWhitespaces();
            getToken(TokenType.PARENTHESIS_CLOSE);

        } else {
            if (currentToken.getTokenType() == TokenType.AT) {
                variable_representation();
            }
            else {
                // transaction- if doesn't work-> return to previous state
                int tokensHeadBackup = saveState();
                try {
                    mixin_call();
                } catch (ParseException e1) {
                    restoreState(tokensHeadBackup);
                    try {
                        accessor();
                    } catch (ParseException e2) {
                        restoreState(tokensHeadBackup);
                        value();
                    }
                }
            }
            //if OKAY, check if any operation sign (+, -, * etc. comes after)
            int tokensHeadBackup = saveState();
            try {
                optionalWhitespaces();
                operation_section();
            } catch( ParseException e){
                restoreState(tokensHeadBackup);
                // OK, just not an operation
            }

        }
    }



    private void hex_value() throws ParseException {
        getToken(TokenType.HASH);
        popMatchingTokensRequired(TokenType.LETTER, TokenType.DIGIT);
        //TODO: a-fA-F only
    }

    private void number() throws ParseException{
        popMatchingTokensRequired(TokenType.DIGIT); // allow whitespaces also?
        if (currentToken.getTokenType() == TokenType.DOT){
            getToken(TokenType.DOT);
            popMatchingTokens(TokenType.DIGIT); // 3. is also valid now, if 3.0 should be only valid: use popMatchingTokensRequired()
        }
    }

    private void text_outside_string() throws ParseException{
        name();
    }

    private void value() throws ParseException {
        if( currentToken.getTokenType() == TokenType.HASH ){
            hex_value();
        } else if (currentToken.getTokenType() == TokenType.DIGIT ){
            number();
            int tokensHeadBackup = saveState();
            try{
                getToken(TokenType.UNIT);
            } catch(ParseException e){
                restoreState(tokensHeadBackup);
                // no unit
            }
        } else if (currentTokenTypeIsIn(new TokenType[]{TokenType.SINGLE_QUOTE, TokenType.DOUBLE_QUOTE})){
            string_text();
        } else if (currentTokenTypeIsIn(new TokenType[]{TokenType.LETTER, TokenType.UNDERSCORE})){
            text_outside_string();
        } else {
            throw new ParseException(currentToken, TokenType.HASH, TokenType.DIGIT, TokenType.SINGLE_QUOTE, TokenType.DOUBLE_QUOTE, TokenType.LETTER, TokenType.UNDERSCORE);
        }

    }

    private void operation_section() throws ParseException {
        operator();
        optionalWhitespaces();
        r_value();
    }

    private void operator() throws ParseException{
        getAnyOfTokens(TokenType.PLUS, TokenType.MINUS, TokenType.ASTERISK, TokenType.SLASH);
    }

    private void accessor() throws ParseException {
        selectors();
        // optionalWhitespaces(); // should it be here? "tag['name']" only or  or "tag ['name']" as well?
        accessor_open();
        l_value();
        accessor_close();
    }


    private void accessor_open() throws ParseException{
        getToken(TokenType.BRACES_OPEN);
        optionalWhitespaces();
        getToken(TokenType.SINGLE_QUOTE);
    }
    private void accessor_close() throws ParseException{
        getToken(TokenType.SINGLE_QUOTE);
        optionalWhitespaces();
        getToken(TokenType.BRACES_CLOSE);
    }


    private void l_value() throws ParseException{
        if ( currentToken.getTokenType() == TokenType.AT ){
            variable_representation();
        } else {
            property_name();
        }
    }

    private void property_name() throws ParseException{
        name();
    }


    private void selectors() throws ParseException{
        if ( currentToken.getTokenType() == TokenType.ASTERISK ){
            getToken(TokenType.ASTERISK);
            optionalWhitespaces();
            attribute_filters();
            optionalWhitespaces();
            pseudo_classes();
        } else {
            selectors_base();
        }
        pseudo_elements();
    }


    private void attribute_filters() { // may recurse infinitely as filters may be combined (?)
        int tokensHeadBackup = saveState();
        try{
            attribute_filter();
            attribute_filters();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            //OK, eps
        }
    }

    private void attribute_filter() throws ParseException {
        attribute_open();
        optionalWhitespaces();
        attribute_name();
        optionalWhitespaces();
        attribute_compare();
        optionalWhitespaces();
        r_value();
        optionalWhitespaces();
        attribute_close();
    }

    private void attribute_open() throws ParseException {
        getToken(TokenType.BRACES_OPEN);
    }

    private void attribute_close() throws ParseException {
        getToken(TokenType.BRACES_CLOSE);
    }

    private void attribute_name() throws ParseException{
        name();
    }

    private void attribute_compare() throws ParseException{
        if ( currentTokenTypeIsIn(new TokenType[]{TokenType.ASTERISK, TokenType.TILDE, TokenType.PIPE, TokenType.CARET, TokenType.DOLLAR})){
           getToken(currentToken.getTokenType());
        }
        getToken(TokenType.EQUALS);
    }

    private void pseudo_classes() {
        if ( currentToken.getTokenType() == TokenType.COLON ) {
            int tokensHeadBackup = saveState();
            try {
                pseudo_class_parametrized();
                pseudo_classes();
            } catch (ParseException e1) {
                restoreState(tokensHeadBackup);
                try {
                    pseudo_class();
                    pseudo_classes();
                } catch (ParseException e2) {
                    restoreState(tokensHeadBackup);
                    // OK, optional
                }
            }
        }
    }

    private void pseudo_class_parametrized() throws ParseException{
        getToken(TokenType.COLON);
        pseudo_class_parametrized_name();
        getToken(TokenType.PARENTHESIS_OPEN);
        optionalWhitespaces();
        pseudo_class_parameter();
        optionalWhitespaces();
        getToken(TokenType.PARENTHESIS_CLOSE);
    }

    private void pseudo_class_parametrized_name() throws ParseException{
        name();
    }

    private void pseudo_class_parameter() {
        int tokensHeadBackup = saveState();
        try {
            selectors();
        } catch (ParseException e1){
            restoreState(tokensHeadBackup);
            try {
                r_values();
            } catch (ParseException e2) {
                restoreState(tokensHeadBackup);
                    attribute_filters();
                    //may be eps as attribute_filters may be eps
            }
        }

    }

    private void pseudo_class() throws ParseException{
        getToken(TokenType.COLON);
        pseudo_class_name();

    }
    private void pseudo_class_name() throws ParseException {
        name();
    }

    private void selectors_base() throws ParseException{
        selector_name();
        attribute_filters();
        pseudo_classes();
        selectors_continuation();
    }

    private void selector_name() throws ParseException{
        int tokensHeadBackup = saveState();
        try{
            tag_name();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            try{
                class_name();
            } catch(ParseException e2){
                restoreState(tokensHeadBackup);
                id_name();
            }
        }
    }

    private void tag_name() throws ParseException{
        name();
    }

    private  void class_name() throws ParseException{
        getToken(TokenType.DOT);
        name();
    }

    private void id_name() throws ParseException{
        getToken(TokenType.HASH);
        name();
    }

    private void selectors_continuation() {
        int tokensHeadBackup = saveState();
        try{
            selectors_separator();
            selectors_base();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            //eps
        }
    }

    private void selectors_separator() throws ParseException{
        int tokensHeadBackup = saveState();
        try{
            optionalWhitespaces();
            getAnyOfTokens(TokenType.PLUS, TokenType.TILDE, TokenType.SHARP_CLOSE, TokenType.COMMA);
            optionalWhitespaces();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            requiredWhitespaces();
        }
    }

    private void pseudo_elements() { // may recurse infinitely, otherwise add limit e.g. 100x
        int tokensHeadBackup = saveState();
        try{
            pseudo_element_sign();
            pseudo_element_name();
            pseudo_elements();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            //eps
        }
    }

    private void pseudo_element_sign() throws ParseException{
        getToken(TokenType.COLON);
        getToken(TokenType.COLON);
    }

    private void pseudo_element_name() throws ParseException{
        name();
    }


    private void mixin_call() throws ParseException{
        mixin_class_selector();
        getToken(TokenType.PARENTHESIS_OPEN);
        optionalWhitespaces();
        int tokensHeadBackup = saveState();
        try{
            arguments();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            // OK, no arguments
        }
        optionalWhitespaces();
        getToken(TokenType.PARENTHESIS_CLOSE);
    }

    private void mixin_class_selector() throws ParseException{
        selector_name();
    }

    private void arguments() throws ParseException{
        r_value();
        arguments_continuation();
    }

    private void arguments_continuation() {
        int tokensHeadBackup = saveState();
        try{
            optionalWhitespaces();
            getToken(TokenType.COMMA);
            optionalWhitespaces();
            arguments();
        } catch( ParseException e){
            restoreState(tokensHeadBackup);
            //eps
        }
    }

    private void inline_comment() throws ParseException{
        getToken(TokenType.INLINE_COMMENT_OPEN);
        text_in_inline_comment();
        if ( currentToken.getTokenType() == TokenType.EOL ){
            getToken(TokenType.EOL);
        } else{
            getToken(TokenType.EOF);
        }
    }

    private void text_in_inline_comment() throws ParseException{
        TokenType[] acceptedTokens = Tokenizer.getAllTokenTypesBut(TokenType.EOL, TokenType.EOF);
        popMatchingTokens(acceptedTokens);
    }

    private void block_comment() throws ParseException{
        getToken(TokenType.BLOCK_COMMENT_OPEN);
        text_in_block_comment();
        getToken(TokenType.BLOCK_COMMENT_CLOSE);
    }

    private void text_in_block_comment() throws ParseException{
        TokenType[] acceptedTokens = Tokenizer.getAllTokenTypesBut(TokenType.EOF, TokenType.BLOCK_COMMENT_CLOSE);
        popMatchingTokens(acceptedTokens);
    }

    private void element_style() throws ParseException{
        int tokensHeadBackup = saveState();
        try {
            selectors();
        } catch(ParseException e1){
            restoreState(tokensHeadBackup);
            try{
                mixin_class_selector();
                parameters();
            } catch(ParseException e2){
                restoreState(tokensHeadBackup);
            }
        }
        optionalWhitespaces();
        declaration();
    }

    private void parameters() throws ParseException{
        getToken(TokenType.PARENTHESIS_OPEN);
        optionalWhitespaces();
        int tokensHeadBackup = saveState();
        try{
            parameters_definition();
            optionalWhitespaces();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            //ok, optional
        }
        getToken(TokenType.PARENTHESIS_CLOSE);
    }

    private void parameters_definition() throws ParseException {
        parameter_definition();
        optionalWhitespaces();
        parameters_definition_continuation();
    }

    private void parameter_definition() throws ParseException {
        variable_representation();
        optionalWhitespaces();
        if( currentToken.getTokenType() == TokenType.COLON ){
            getToken(TokenType.COLON);
            r_value();
        }
    }

    private void parameters_definition_continuation() throws ParseException {
        if (currentToken.getTokenType() == TokenType.COMMA) {
            getToken(TokenType.COMMA);
            optionalWhitespaces();
            parameters_definition();
        }
        // else eps
    }

    private void declaration() throws ParseException {
        getToken(TokenType.BRACKETS_OPEN);
        optionalWhitespaces();
        rules();
        optionalWhitespaces();
        getToken(TokenType.BRACKETS_CLOSE);
    }

    private void rules(){ // may recurse infinitely as rules may contain rules
        int tokensHeadBackup = saveState();
        try {
            rule();
            optionalWhitespaces();
            rules();
        } catch(ParseException e){
            restoreState(tokensHeadBackup);
            // eps
        }
    }

    private void rule() throws ParseException{
        int tokensHeadBackup = saveState();
        try{
            l_value();
            optionalWhitespaces();
            getToken(TokenType.COLON);
            optionalWhitespaces();
            r_values();
            optionalWhitespaces();
            getToken(TokenType.SEMICOLON);
        } catch( ParseException e1 ){
            restoreState(tokensHeadBackup);
            try{
                element_style();
            } catch( ParseException e2 ){
                restoreState(tokensHeadBackup);
                try{
                    mixin_call();
                    optionalWhitespaces();
                    getToken(TokenType.SEMICOLON);
                } catch( ParseException e3 ){
                    restoreState(tokensHeadBackup);
                    try{
                        selectors();
                        optionalWhitespaces();
                        getToken(TokenType.SEMICOLON);
                    } catch( ParseException e4 ){
                        restoreState(tokensHeadBackup);
                        try{
                            pseudo_class_declaration();
                        } catch(ParseException e5){
                            restoreState(tokensHeadBackup);
                            if (currentToken.getTokenType() == TokenType.AT) {
                                variable_definition();
                            } else if (currentToken.getTokenType() == TokenType.INLINE_COMMENT_OPEN) {
                                inline_comment();
                            } else if (currentToken.getTokenType() == TokenType.BLOCK_COMMENT_OPEN) {
                                block_comment();
                            } else {
                                throw new ParseException(currentToken);
                                // not a valid rule: will be looking for } (rules in declaration end)
                            }

                        }
                    }
                }
            }
        }

    }

    private void pseudo_class_declaration() throws ParseException{
        getToken(TokenType.AMPERSAND);
        pseudo_classes();
        pseudo_elements();
        optionalWhitespaces();
        declaration();
    }

}