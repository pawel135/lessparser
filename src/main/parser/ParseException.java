package main.parser;

import main.tokenizer.Token;
import main.tokenizer.TokenType;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParseException extends Throwable {
    final Token unexpectedToken;
    final TokenType[] expectedTokenTypes;

    public ParseException(Token currentToken, TokenType... expectedTokenTypes){
        unexpectedToken = currentToken;
        this.expectedTokenTypes = expectedTokenTypes;
    }

    @Override
    public String toString(){
        return "Parse error. Unexpected token from position " + unexpectedToken.getStart() + " to " + unexpectedToken.getEnd() + " (" + unexpectedToken.getTokenType().toString() + ")"+
                "\n Expected " + Arrays.stream(expectedTokenTypes).map(TokenType::toString).collect(Collectors.joining(" or ")) +" instead.";
    }

    public String toString(String text){
        int errorStart = unexpectedToken.getStart();
        int errorEnd = unexpectedToken.getEnd();
        errorStart = Math.min(text.length(), Math.max(0, errorStart)) ;
        errorEnd = Math.min(text.length(), Math.max(0, errorEnd)) ;

        Function<Integer, Long> rowOfPosition = position -> (IntStream.range(0, position).filter(i -> text.charAt(i)=='\n').count());
        Function<Integer, Integer> firstCharInPositionRow = position -> IntStream.range(0, position).filter(i ->text.charAt(i) == '\n').max().orElse(-1)+1; // +1 for next char after EOL and +1 for numbering starting from 1

        // +1's are for numbering starting from 1
        long rowStart = rowOfPosition.apply(errorStart) + 1;
        long colStart = errorStart - firstCharInPositionRow.apply(errorStart) +1;
        long rowEnd = rowOfPosition.apply(errorEnd) + 1;
        long colEnd = errorEnd - firstCharInPositionRow.apply(errorEnd) + 1;

        return String.format("Parse error. Unexpected token \"%s\" from position %d (row: %d, col: %d) to position %d (row: %d, col: %d).\n Expected %s instead",
                unexpectedToken.getTokenType().toString(), errorStart, rowStart, colStart,    errorEnd, rowEnd, colEnd,
                Arrays.stream(expectedTokenTypes).map(TokenType::toString).collect(Collectors.joining(" or ")) );
    }

    public int getUnexpectedStart(){
        return unexpectedToken.getStart();

    }
    public int getUnexpectedEnd(){
       return unexpectedToken.getEnd();
    }

}


