package main.scanner;

import main.tokenizer.Token;
import main.tokenizer.Tokenizer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Scanner {
    private String filename;

    public Scanner(String filename) throws FileNotFoundException{
        if ( !Files.exists(Paths.get(filename)) ){
            throw new FileNotFoundException();
        }
        this.filename = filename;
    }

    public String[] getFileLines() {
        try(FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr)){
            return br.lines().toArray(String[]::new);
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public String getFileContentAsString() {
        try(FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr)){
            return br.lines().collect(Collectors.joining("\n"));
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public List<Token> getTokens(){
        Tokenizer tokenizer = new Tokenizer( getFileContentAsString() );
        return tokenizer.getTokens();
    }


}
