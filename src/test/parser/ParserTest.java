package test.parser;

import main.parser.ParseException;
import main.parser.Parser;
import main.tokenizer.Token;
import main.tokenizer.TokenType;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class ParserTest {

    public static final String TEST_STRING = "@import \"something\"\n"+
            "@base:#f04615; \n"+
            " @width:0.5; .class { width: percentage(@width); // returns `50%`\n"+
            "color: saturate(@base, 5%); background-color: spin(lighten(@base, 25%), 8);\n"+
            "}\n"+
            "/* One hell of a block style comment! */@var: red; // Get in line!\n"+
            "@var: white;﻿";

    @Test(expected = ParseException.class )
    public void testParse() throws ParseException{
        List<Token> tokens = new ArrayList<>();
        tokens.add(new Token(TokenType.IMPORT_KEYWORD,0,7));
        tokens.add(new Token(TokenType.WHITESPACES,7,8));
        Parser parser = new Parser(tokens);
        parser.parse();
    }
}
