package test.tokenizer;

import main.tokenizer.Tokenizer;
import org.junit.Test;

public class TokenizerTest {
    private static final String TEST_STRING = "@import \"something\"\n"+
            "@base:#f04615; \n"+
            " @width:0.5; .class { width: percentage(@width); // returns `50%`\n"+
            "color: saturate(@base, 5%); background-color: spin(lighten(@base, 25%), 8);\n"+
            "}\n"+
            "/* One hell of a block style comment! */@var: red; // Get in line!\n"+
            "@var: white;﻿";
    @Test
    public void testSth() {
        Tokenizer tokenizer = new Tokenizer(TEST_STRING);
        System.out.println( tokenizer.getTokens() );
    }


}
