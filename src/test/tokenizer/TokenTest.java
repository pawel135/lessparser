package test.tokenizer;

import main.tokenizer.Token;
import main.tokenizer.TokenType;
import main.tokenizer.Tokenizer;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.TestCase.assertEquals;

public class TokenTest {
    private static String testString;
    @Before
    public void setUp(){
        testString = "@import something;\n"+
                "@base:#f04615; \n"+
                " @width:0.5; .class { width: percentage(@width); // returns `50%`\n"+
                "color: saturate(@base, 5%); background-color: spin(lighten(@base, 25%), 8);\n"+
                "}\n"+
                "/* One hell of a block style comment! */@var: red; // Get in line!\n"+
                "@var: white;﻿";
    }

    @Test
    public void testImportKeyword() {
        assertEquals(1, countOccurrences(TokenType.IMPORT_KEYWORD));
    }

    @Test
    public void testVariableSign(){
        assertEquals(8, countOccurrences(TokenType.AT));

    }

    @Test
    public void testAllTerminalOperators(){
        TokenType[] tokenTypes = Tokenizer.getAllTokenTypes();
        for( TokenType tokenT : tokenTypes ){
            System.out.println( countOccurrences(tokenT) );
        }

    }

    private int countOccurrences(TokenType tokenType){
        Token token = new Token(tokenType,0,0);
        Pattern pattern = token.getPattern();
        Matcher matcher = pattern.matcher(testString);
        int occurrences = 0;
        while (matcher.find()) {
            token.setStart(matcher.start());
            token.setEnd(matcher.end());
            System.out.println("FOUND " + matcher.group() + ", start: " + matcher.start() + ", end: "+ matcher.end() );
            occurrences++;
        }
        return occurrences;

    }
}
